<!DOCTYPE html>
<html lang="fr">

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Test Uuu</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

  <link rel="stylesheet" href="css/style.css">

  <style>
    .ok {
      color: red;
    }
  </style>
</head>

<body>
  <div class="ok"><h1>Ok 12345</h1></div>
  <p>Yes Nouveau test</p>
  <p>Voilà un <b style="color: #cf33ff;">nouveau</b><span style="color: #33ff33;">
    </span>paragraphe2 t<i>ati</i></p>
  <p style="text-align: justify;">Lorem ipsum dolor, sit amet consectetur adipisicing.</p>

  <hr>

  <p><img src="../../themes/c57/assets/images/conf_laragon.png" alt="Capture" title="Capture" style="width: 100px;"> </p>
  <p>Voilà. Yes</p>
  <!--
<i class="far fa-1x fa-smile"></i>
<i class="far fa-2x fa-smile"></i>
<i class="far fa-3x fa-smile"></i>
<i class="far fa-4x fa-smile"></i>
<i class="far fa-5x fa-smile"></i>
<i class="far fa-6x fa-smile"></i>
<i class="far fa-7x fa-smile"></i>
<i class="far fa-8x fa-smile"></i>
<i class="far fa-9x fa-smile"></i>
<i class="far fa-10x fa-smile-wink"></i>
-->
  <hr>
  <i style="vertical-align: middle;" class="fa fa-2x fa-code"></i>

  <?= "okiiiiiiiiiiiiiiiii ça, c'est du PHP ! ;-)  ";

?>
  <hr>
  Ok.

</html>
