<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Profile;

use GrCOTE7\Profile\Models\Profile as ProfileModel;
use RainLab\User\Controllers\Users as UsersController;
use RainLab\User\Models\User as UserModel;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Profile',
            'description' => 'Profile du membre de c57.',
            'author'      => 'GrCOTE7',
            'icon'        => 'icon-graduation-cap',
        ];
    }

    public function registerComponents()
    {
        return [
            'GrCOTE7\Profile\Components\UsersList' => 'userslist',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        UserModel::extend(function ($model) {
            // $model->addFillable([
            //     'pseudo', 'parr', 'sexe',
            // ]);
            $model->hasOne['profile'] = ['GrCOTE7\Profile\Models\Profile'];
        });

        UsersController::extendFormFields(function ($form, $model, $context) {
            if (!$model instanceof UserModel) {
                return;
            }
            if (!$model->exists) {
                return;
            }

            // Ensure that a profile model always exists...
            ProfileModel::getFromUser($model);

            $form->addTabFields([
                'profile[pseudo]' => [
                    'label' => 'Pseudo',
                    'type'  => 'text',
                    'tab'   => 'Profile',
                ],
                'profile[parr]' => [ // email à terme
                    'label' => 'Parrain',
                    'type'  => 'text',
                    'tab'   => 'Profile',
                ],
                'profile[sexe]' => [
                    'label'   => 'Sexe',
                    'tab'     => 'Profile',
                    'type'    => 'dropdown',
                    'options' => ['Inconnu', 'H', 'F'],
                ],
            ]);
        });
    }
}
