<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Profile\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

// AddNewFields
class AddNewFields extends Migration
{
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('facebook')->nullable();
            $table->text('bio')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn([
                'facebook',
                'bio',
            ]);
        });
    }
}
// php artisan plugin:refresh grcote7.profile
