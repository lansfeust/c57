<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Profile\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class create_profiles_table extends Migration
{
    public function up()
    {
        Schema::create('grcote7_profile_profiles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('pseudo')->nullable;
            $table->string('parr')->nullable;
            $table->enum('sexe', ['Inconnu', 'H', 'F'])->default('Inconnu');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grcote7_profile_profiles');
    }
}
