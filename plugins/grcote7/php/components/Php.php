<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Php\Components;

use Cms\Classes\ComponentBase;

class Php extends ComponentBase
{
    protected $result;

    public function componentDetails()
    {
        return [
            'name'        => 'Php Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        /*
        $divers   = new Divers();
        $result[] = $divers->init();

        // Méthode avec closure builder
        $table    = new TableDe();
        $result[] = $table->de5;

        // Méthode normale
        $result[] = new TableDe(7);

        */

        // $result[] = User::oneUser();

        $result[] = $this->someUsers();

        // $result[] = $this->lotUsers();

        $this->result = $result;
        $this->aff();
    }

    public function someUsers()
    {
        $users = new User();

        return $users->someUsers(3);
    }

    public function lotUsers()
    {
        return User::lotUsers();
    }

    protected function aff()
    {
        $this->page['data'] = print_r($this->result, true);
    }
}
