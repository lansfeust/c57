<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
  public function pluginDetails()
  {
    return [
      'name'        => 'Demos',
      'description' => 'Une séries de démos de petites components.',
      'author'      => 'GC7',
      'icon'        => 'icon-hand-o-up',
    ];
  }

  public function registerComponents()
  {
    return [
      'GrCOTE7\Demos\Aff\Components\HomeD'            => 'homeD',
      'GrCOTE7\Demos\Aff\Components\AffTri'           => 'affTri',
      'GrCOTE7\Demos\Aff\Components\SimpleForm'       => 'simpleForm',
      'GrCOTE7\Demos\Divers\Meteo\Components\Meteo'   => 'meteo',
      'GrCOTE7\Demos\Divers\Todoc57\Components\Taf'   => 'tAF',
      'GrCOTE7\Demos\Divers\Ajax\Components\Ajax'     => 'ajax',
      'GrCOTE7\Demos\Divers\Ajax\Components\AjaxForm' => 'ajaxForm',
      'GrCOTE7\Demos\Divers\Ajax\Components\BtnYN'    => 'btnYN',
    ];
  }
}