<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos\Aff\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Mail;

class SImpleForm extends ComponentBase
{
  public $reponse;

  public function componentDetails()
  {
    return [
      'name'        => 'Simple Form',
      'description' => 'Simple formulaire + envoi d\'un email',
    ];
  }

  public function onRun()
  {
    $this->addCss(['components/simpleform/assets/css/alertes.less']);
  }

  public function onSend()
  {
    // mail('tttt@ggg.com', 'sujet', 'okioki');

    // $vars = ['origin'=>'oOo', 'name' => 'Joe', 'user' => 'Mary'];
    // Mail::send('grcote7.contact::mail.message', $vars, function ($message) {
    //   $message->to('contact@asbl-vap.be', 'Admin Person');
    //   $message->subject('This is a reminder');
    // });

    $this->reponse = json_encode($_POST);
  }
}