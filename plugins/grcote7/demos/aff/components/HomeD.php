<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos\Aff\Components;

use Cms\Classes\ComponentBase;

class HomeD extends ComponentBase
{
  public function componentDetails()
  {
    return [
      'name'        => 'Home T',
      'description' => 'Affichage d\'un icône Home en haut à droite (Fixe) pour D, pages des démos.',
    ];
  }

  public function onRun()
  {
    $this->addCss('components/homed/assets/css/style.css');
    // This code will note execute AJAX events.
    // {{ demoAff.name }} (strict) => Never conflict
    $this->name = 'Lionel';
    $this->role = 'Admin';
    // {% set name = name %}
    //@v oki
    //@q exemple
    //! juste pour test colors
    // {{ name }} (Relaxed)
    $this->page['homeT'] = 'HomeT';
  }
}