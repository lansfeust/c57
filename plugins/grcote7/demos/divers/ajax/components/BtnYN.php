<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos\Divers\Ajax\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Support\Facades\Flash;

class BtnYN extends ComponentBase
{
  public $reponse;

  public function componentDetails()
  {
    return [
      'name'        => 'BtnYN',
      'description' => '2 boutons Oui/Non Ajax',
    ];
  }

  /*
  public function onRender()
  {
  $this->var = $this->property('var'); // Récup param d'appel du component
  debug($this->var);
  }
  */

  public function onRun()
  {
    \Debugbar::enable();
    // $this->addCss(['components/w/assets/css/style.css']);
  }

  public function onClick()
  {
    // $this->reponse = key(Input());
    // $this->reponse = key(post());
    $this->reponse = post('id') ? 'oui':'non';

    debug(post('id'));
    debug('Ajax response: '.$this->reponse);

    Flash::info('Informations reçues.<hr>À la question "Une question ?", vous avez répondu "'.ucfirst($this->reponse.'"'));
  }
}