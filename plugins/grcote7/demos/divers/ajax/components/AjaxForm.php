<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos\Divers\Ajax\Components;

use Cms\Classes\ComponentBase;
use Flash;
use ValidationException;
use Validator;

class AjaxForm extends ComponentBase
{
  public $var;

  public function componentDetails()
  {
    return [
      'name'        => 'Ajax Form',
      'description' => 'Simple formulaire Ajax (Avec validation)',
    ];
  }

  // public function onRender()
  // {
  //   $this->var = $this->property('var'); // Récup param d'appel du component
  //   debug($this->var);
  // }

  public function onRun()
  {
    \Debugbar::enable();
    // $this->addCss(['components/w/assets/css/style.css']);
    $this->var = $this->getResponse();
  }

  public function getResponse()
  {
    // return 'ready.';
  }

  public function onDoSomething()
  {
    $data = post();

    $rules = [
      'name'  => 'required',
      'email' => 'required|email',
    ];

    $validation = Validator::make($data, $rules);

    if ($validation->fails()) {
      throw new ValidationException($validation);
    }
    Flash::success('Jobs done!');
  }
}