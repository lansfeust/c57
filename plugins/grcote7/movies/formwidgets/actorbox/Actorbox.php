<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Movies\FormWidgets;

use Backend\Classes\FormWidgetBase;

class Actorbox extends FormWidgetBase
{
  public function widgetDetails()
  {
    return [
      'name'        => 'Actorbox',
      'description' => 'Field for adding actors',
    ];
  }

  public function render()
  {
    return $this->makePartial('widget');
  }

  public function loadAssets()
  {
    $this->addCss('css/select2.css');
    $this->addJs('js/select2.js');
  }
}