<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Dpt\Components;

use Cms\Classes\ComponentBase;
use Grcote7\Dpt\Models\Dpt;

class DptList extends ComponentBase
{
  public $records;

  public function componentDetails()
  {
    return [
      'name'        => 'grcote7.dpt::lang.components.dptlist.name',
      'description' => 'grcote7.dpt::lang.components.dptlist.description',
    ];
  }

  public function onRun()
  {
    $this->page['titreList'] = 'The listing des départements français';
    $this->page['name']      = 'La fameuse liste';

    $this->records = Dpt::all();
    // var_dump($this->records);
    // $this->records = '';
  }
}