<?php

use GrCOTE7\Dpt\Models\Dpt;

/*
 * Ce fichier est la propriété de C57.fr
 *' .
              ' * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

Route::get('ww', function () {

  return Dpt::orderBy('code', 'desc')->paginate(5);
});