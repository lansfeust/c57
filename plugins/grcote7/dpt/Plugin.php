<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Dpt;

use System\Classes\PluginBase;

// The plugin.php file (called the plugin initialization script) defines the plugin information class.
    /**
     * Mon plugin Dpt.
     */
    class Plugin extends PluginBase
    {
      public function pluginDetails()
      {
        return [
          'name'        => 'grcote7.dpt::lang.plugin.name',
          'description' => 'grcote7.dpt::lang.plugin.description',
          'author'      => 'GC7',
          'icon'        => 'oc-icon-list-ol',
        ];
      }

      public function registerComponents()
      {
        return [
          'GrCOTE7\Dpt\Components\DptList' => 'dptList',
          'GrCOTE7\Dpt\Components\DptName' => 'dptName'
        ];
      }
    }