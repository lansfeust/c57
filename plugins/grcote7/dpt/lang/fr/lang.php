<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

return [
    'plugin' => [
        'name'        => 'dpt',
        'description' => 'Fonctions sur les départements',
    ],
    'components' => [
        'dptlist' => [
            'name'        => 'Les départements',
            'description' => 'Listing',
        ],
        'dptname' => [
            'name'        => 'Nom d\'un département',
            'description' => 'Selon son numéro',
        ],
    ],
];