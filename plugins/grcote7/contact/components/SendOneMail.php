<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Contact\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Mail;

class SendOneMail extends ComponentBase
{
  public $mavar;

  public function componentDetails()
  {
    return [
      'name'        => 'Send One Mail',
      'description' => 'Simple sending of one email for test',
    ];
  }

  public function onRun()
  {
    $this->mavar = [123, 'abc', 789, ''];

    $gc7  = 'grcote7@gmail.com';
    $vap  = 'admin@asbl-vap.be';
    $name = 'Admin';
    // $dest = env('MAIL_USERNAME', 'Your email');
    $dest = $vap;
    $tpl  = 'grcote7.contact::mail.emailTest';

    $params = [
      'owner'   => env('OWNER', 'You'),
      'subject' => 'Le sujet',
      'origin'  => 'Test avec component SendOneMail',
      'name'    => 'Lionel',
      'email'   => 'expediteur@email.com',
      'content' => 'Du contenu ici....',
    ];

    Mail::send($tpl, $params, function ($message) use ($dest) {
      $message->to($dest);
      $message->subject('De la part de Testing email');
    });

    // mail($vap, 'Sujet', 'contenu');

    // Mail::send($tpl, $params, function($message) use ($dest){
    //   $message->to($dest);
    // });

    // Signature sendTo
    // Mail::sendTo($recipient, $message, $params, $callback, $options);
    // Mail::sendTo($dest, $tpl, $params);
    // Mail::sendTo($user, $tpl, $params); // $user doit être un objet
    // Mail::sendTo([$dest=>$name], $tpl, $params);

    // SImple mail sans autres infos
    // Mail::rawTo($dest, 'Hello friend');
    // Il existe:
    // Mail::rawTo([$dest => $name, $gc7 => 'Lio'], ['subject'=>'ooo']);

    // OK
    // Mail::sendTo('contact@asbl-vap.be', 'grcote7.contact::mail.message', $params);

    $this->mavar[] = 'Script éxécuté !';
    $this->mavar[] = $dest ?? 'Indiqué NULL';

  }
}