<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Vehicules\Components;

use Cms\Classes\ComponentBase;
use Grcote7\Vehicules\classes\Vehicule;

class Vehicules extends ComponentBase
{
  public function componentDetails()
  {
    return [
      'name'        => 'Vehicule',
      'description' => 'Dev classe Vehicule',
    ];
  }

  public function onRun()
  {
    $this->addCss('assets/css/style.css');
    $this->page['vehicule'] = $this->initVehicule();
    // $this->unePropriete=123456; // N'est pas transmise au template
  }

  public function initVehicule()
  {
    $unVehicule = new Vehicule();

    $unVehicule->nouvellePropriete = 789;

    $unVehicule->setOwner('Li');
    $unVehicule->rouler(50);

    return $unVehicule;
  }
}