<?php namespace Grcote7\C57\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'grcote7_c57_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}