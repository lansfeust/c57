<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\C57\Components;

use Cms\Classes\ComponentBase;
use RainLab\User\Models\User;

class DerInscrits extends ComponentBase
{
    public $derinscrits;

    public function componentDetails()
    {
        return [
            'name'        => 'Derniers Inscrits',
            'description' => 'Affiche la liste des derniers inscrits.',
        ];
    }

    public function onRun()
    {
        $this->addCss('assets/css/derInscrits.css');
        $this->derinscrits = $this->loadDerInscrits();
        // var_dump($this->derinscrits);
    }

    protected function loadDerInscrits()
    {
        return User::all()->sortByDesc('created_at')->take(7);
    }
}