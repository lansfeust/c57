<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Rafie\Blogplus;

use Backend;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;

/**
 * blogplus Plugin Information File.
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'blogplus',
            'description' => 'No description provided yet...',
            'author'      => 'rafie',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register()
    {
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Rainlab\Blog\Models\Post::extend(function ($model) {
            $model->addDynamicMethod('scopeTrashed', function ($query) {
                return $query->where('deleted_at', '=', null);
            });
        });

        Event::listen('backend.list.extendColumns', function ($widget) {
            // Only for the Posts controller
            if ((!$widget->getController() instanceof \Rainlab\Blog\Controllers\Posts)) {
                return;
            }

            $widget->addColumns([
                'deleted_at' => [
                    'label' => 'Deleted',
                    'type'  => 'date',
                ],
            ]);
        });

        Event::listen('backend.filter.extendScopes', function ($widget) {
            // Only for the Posts controller
            if ((!$widget->getController() instanceof \Rainlab\Blog\Controllers\Posts)) {
                return;
            }

            $widget->addScopes([
                'Trashed' => [
                    'label'      => 'Hide trashed',
                    'type'       => 'checkbox',
                    'conditions' => 'deleted_at IS NULL',
                ],
            ]);
        });

        Event::listen('eloquent.deleting: RainLab\Blog\Models\Post', function ($record) {
            $record->deleted_at = Carbon::now();
            $record->save();

            return false;
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate
        return [
            'Rafie\Blogplus\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate
        return [
            'rafie.blogplus.some_permission' => [
                'tab'   => 'blogplus',
                'label' => 'Some permission',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate
        return [
            'blogplus' => [
                'label'       => 'blogplus',
                'url'         => Backend::url('rafie/blogplus/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['rafie.blogplus.*'],
                'order'       => 500,
            ],
        ];
    }
}
