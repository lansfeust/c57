<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Rafie\BlogPlus\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class create_posts_deleted_at_field extends Migration
{
    public function up()
    {
        Schema::table('rainlab_blog_posts', function ($table) {
            $table->timestamp('deleted_at')
                ->default(null)
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('rainlab_blog_posts', function ($table) {
            $table->dropColumn('deleted_at');
        });
    }
}
