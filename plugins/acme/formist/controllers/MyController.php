<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Acme\Formist\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\Backend;
use BackendMenu;
use Flash;

class MyController extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController',
    ];

    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Acme.Formist', 'formist', 'mycontroller');
    }

    // Nécessite d'utiliser: $widget->render()  dans le partial
    // Là, si index non défini, c'est le behavior par défaut qui est utilisé
    // Pose les boutons en haut de la liste si ss le controller, list_toolbar est fourni et renseigné dans config_list
    public function uuu_index()
    {
        // Return a simple string
        // return 'Home';

        // Return a redirect (Ici, va aller utiliser la méthode helloworld())
        // return Backend::redirect('acme/formist/mycontroller/helloworld');
        //
        debug('Voir code pour décommenter lignes début index()');

        $config = $this->makeConfig('$/acme/formist/models/customer/columns.yaml');

        $config->model = new \Acme\Formist\Models\Customer();

        $config->recordUrl = 'acme/formist/mycontroller/update/:id';

        $widget = $this->makeWidget('Backend\Widgets\Lists', $config);

        $widget->bindToController();

        $this->vars['widget'] = $widget;
    }

    public function update($id = null)
    {
        $config = $this->makeConfig('$/acme/formist/models/customer/fields.yaml');

        $config->model = \Acme\Formist\Models\Customer::find($id);

        $widget = $this->makeWidget('Backend\Widgets\Form', $config);

        $this->vars['widget'] = $widget;
    }

    public function onUpdate($id = null)
    {
        $data = post();

        // Check storage/logs/system.log
        trace_log($data);

        Flash::success('Jobs done !');
    }

    public function helloworld($param1 = null, $param2 = null)
    {
        // ... Pourrait être vide => Ne renvoie alors que le partial
        $this->vars['maVar']            = 777;
        $this->vars['mesParamsDansUrl'] = [$param1, $param2];

        Flash::success('Page atteinte !');
    }

    public function onHello($id = null)
    {
        $data = 789;

        // Check storage/logs/system.log
        trace_log($data);

        Flash::success('Jobs done !');
    }
}
