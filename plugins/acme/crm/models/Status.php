<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Acme\Crm\Models;

use Model;

/**
 * Status Model.
 */
class Status extends Model
{
    /**
     * @var string the database table used by the model
     */
    public $table = 'acme_crm_statuses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];
}
